# -*- coding: utf-8 -*-
import numpy as np


def une_somme(a, b):
    return a + b


def une_somme_numpy(a, b):
    return np.array(a) + np.array(b)
