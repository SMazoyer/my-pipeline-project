# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

setup(
    name='pipeline',
    version='0.1.0',
    description='Test project for gitlab pipelines',
    author='Simon Mazoyer',
    license=license,
    packages=find_packages(exclude=('tests', 'docs'))
)
