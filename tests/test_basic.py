# -*- coding: utf-8 -*-
import unittest

import numpy as np

import pipeline


class TestSuite(unittest.TestCase):
    """Basic test cases."""

    def test_absolute_truth_and_meaning(self):
        assert True

    def test_sommme(self):
        assert pipeline.une_somme(1, 2) == 3

    def test_numpy_sommme(self):
        assert pipeline.une_somme_numpy(1, 2) == 3
        assert np.array_equal(pipeline.une_somme_numpy([1, 2], [2, 3]), np.array([3, 5]))


if __name__ == '__main__':
    unittest.main()
